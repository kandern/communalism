extends Node

signal player_ready
signal player_left

var SERVER_PORT = 6969
var MAX_PLAYERS = 4
var players = []
var nextScene = ""
func _ready():
	if OS.has_feature("server"):
		name = "NetworkManager"
		var peer = NetworkedMultiplayerENet.new()
		peer.create_server(SERVER_PORT, MAX_PLAYERS)
		get_tree().connect("network_peer_disconnected", self, "_player_disconnected")
		get_tree().network_peer = peer
	else:
		queue_free()

func _player_disconnected(id):
	print(str(id) + " disconnected")
	var toRemove
	for player in players:
		if player.id ==id:
			toRemove = player
	if players.has(toRemove):
		players.remove(players.find(toRemove))
	for player in players:
		rpc_id(player.id,"player_left",id)
	emit_signal("player_left")

remote func register_player(username):
	players.append({"id":get_tree().get_rpc_sender_id(),"username":username,"ready":false})
	for player in players:
		rpc_id(player.id,"player_joined",players)

remote func ready_up():
	for player in players:
		if player.id==get_tree().get_rpc_sender_id():
			player.ready = true
		rpc_id(player.id,"ready_up",get_tree().get_rpc_sender_id())
	emit_signal("player_ready")
	if players.size()>1:
		for player in players:
			if player.ready==false:
				return
		for player in players:
			rpc_id(player.id,"change_scene","res://Scenes/test.tscn")
