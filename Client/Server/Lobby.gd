extends Node

var NM: Node
var IS_RUNNING: bool = false
var NUM_BALLS: int = 0
func _ready():
	NM = get_node("/root/NetworkManager");
	randomize()
	$Timer.start(1)
	NM.connect("player_ready", self, "player_ready")
	NM.connect("player_left", self, "player_left")

func make_ball():
	if IS_RUNNING:
		var x = rand_range(-49,49)
		var z = rand_range(-49,49)
		var color = Color(randf(),randf(),randf())
		rpc("make_ball",x,z,color,"ball" + str(NUM_BALLS))
		var scene = load("res://Assets/Ball.tscn")
		var ball = scene.instance()
		ball.spawn(x,z)
		ball.set_color(color)
		ball.name = "ball" + str(NUM_BALLS)
		ball.server = true
		add_child(ball)
		NUM_BALLS += 1

func player_ready():
	if NM.players.size()>1:
		for player in NM.players:
			if player.ready==false:
				return
		IS_RUNNING = true

func _physics_process(_delta):
	var balls = []
	if IS_RUNNING:
		for ball in get_tree().get_nodes_in_group("Balls"):
			balls.append({"id":ball.name,"transform":ball.transform})
		rpc("update_balls",balls)

func player_left():
	if NM.players.size() == 0:
		IS_RUNNING = false
		get_tree().call_group("balls","queue_free")
		for ball in get_tree().get_nodes_in_group("Balls"):
			ball.get_parent().remove_child(ball)
			ball.free();
		NUM_BALLS = 0

remote func setTransform(transform):
	for player in NM.players:
		if player.id != get_tree().get_rpc_sender_id():
			rpc_id(player.id,"setTransform",\
					get_tree().get_rpc_sender_id(),transform)
					
remote func request_players():
	rpc_id(get_tree().get_rpc_sender_id(),"create_players",\
		get_tree().get_rpc_sender_id(),NM.players)
