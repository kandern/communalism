extends Spatial

var NM: Node

func _ready():
	NM = get_node("/root/NetworkManager")
	rpc_id(1,"request_players")

remote func make_ball(x,y,color,ball_name):
	var scene = load("res://Assets/Ball.tscn")
	var ball = scene.instance()
	ball.spawn(x,y)
	ball.set_color(color)
	ball.name = ball_name
	add_child(ball)

remote func create_players(id,players):
	$Player.name = str(id)
	for player in players:
		if player.id==id:
			continue
		var scene = load("res://Assets/Puppet.tscn")
		var obj = scene.instance()
		obj.name = str(player.id)
		obj.spawn()
		add_child(obj)

func sendTransform(transform):
	rpc_id(1,"setTransform",transform)
	
remote func setTransform(id,transform):
	get_node(str(id)).setTransform(transform)

remote func update_balls(balls):
	for ball in balls:
		get_node(ball.id).transform = ball.transform
