extends Node

var NM: Node

func _ready():
	NM = get_node("/root/NetworkManager");
	NM.connect("player_left", self, "player_left")
	NM.connect("player_joined", self, "player_joined")
	NM.connect("connection_successful", self, "connection_Successful")
	NM.connect("ready_up", self, "ready_up")
	$VBoxContainer/VBoxContainer/Control2/txt_Name.text = NM.userName

func on_Quit():
	on_Disconnect()
	get_tree().quit()

func on_Ready():
	$VBoxContainer/VBoxContainer/btn_Ready.disabled = true
	NM.on_ready()

func on_Connect():
	NM.userName = $VBoxContainer/VBoxContainer/Control2/txt_Name.text
	NM.on_Connect()

func on_Disconnect():
	$VBoxContainer/VBoxContainer/btn_Connect.disabled = false
	NM.Disconnect()
	$VBoxContainer/VBoxContainer/btn_Ready.disabled = true

func connection_Successful():
	$VBoxContainer/VBoxContainer/btn_Connect.disabled = true
	$VBoxContainer/VBoxContainer/btn_Ready.disabled = false

func player_left():
	#remove unused users from list
	var list = $VBoxContainer/VBoxContainer2/PlayerList
	for label in list.get_children():
		var remove = true
		for player in NM.players:
			if label.name == str(player.id):
				remove = false
		if remove:
			list.remove_child(label)
			label.queue_free()

#The following functions are executed by the server remotely
func ready_up():
	for player in NM.players:
		for label in $VBoxContainer/VBoxContainer2/PlayerList.get_children():
			if label.name==str(player.id) and player.ready == true:
				label.ready()

func player_joined():
	#Add user to list
	var scene = load("res://Assets/PlayerListItem.tscn")
	var labels = []
	for label in $VBoxContainer/VBoxContainer2/PlayerList.get_children():
		labels.append(label.name)
	for player in NM.players:
		if !labels.has(str(player.id)):
			var newLabel = scene.instance()
			newLabel.name = str(player.id)
			if newLabel.has_method("populate"):
				newLabel.populate(player.username,player.ready)
			$VBoxContainer/VBoxContainer2/PlayerList.add_child(newLabel)

remote func make_ball(_x,_y,_color):
	pass
remote func update_balls(_balls):
	pass
