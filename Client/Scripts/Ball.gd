extends RigidBody
export var size = .5
var server: bool = false
func _ready():
	pass
func spawn(x,z):
	var offset = Vector3(x,20,z)
	set_translation(offset)

func set_color(newColor):
	var material = SpatialMaterial.new()
	material.albedo_color = newColor
	$HitBox/Mesh.set_material_override(material)

func hit(direction):
	rpc_id(1,"get_hit",direction)

remote func get_hit(direction):
	apply_central_impulse(direction)

func sendPosition():
	rpc("getPosition",transform)

remote func getPosition(newTransform):
	transform = newTransform
	
func getSize():
	return size
func _physics_process(_delta):
	if(translation.y<-10):
		spawn(rand_range(-100,100),rand_range(-100,100))
	
