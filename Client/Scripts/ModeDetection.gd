extends Node
func _ready():
	if OS.has_feature("server"):
		print("Launching Server")
		get_tree().change_scene("res://Server/Server.tscn")
	else:
		print("Launching Client")
		get_tree().change_scene("res://Scenes/Lobby.tscn")
