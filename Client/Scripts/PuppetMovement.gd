extends KinematicBody

var velocity = Vector3.ZERO
export var SPEED = 5
var size = 1.0
func _ready():
	spawn()

func onCollision(collision):
	if collision.collider.has_method("hit"):
		collision.collider.hit(velocity)
	
func _physics_process(delta):
	scale.x = lerp(scale.x,size,.01)
	scale.y = lerp(scale.x,size,.01)
	scale.z = lerp(scale.x,size,.01)
	var collisions = move_and_slide(velocity*SPEED,Vector3.UP,false,4,1,false)

	for i in get_slide_count():
		if get_slide_collision(i).collider is StaticBody:
			pass
		else:
			onCollision(get_slide_collision(i))
	if(translation.y<-10):
		spawn()

func setTransform(aTransform):
	transform = aTransform

func getSize():
	return size

func spawn():
	var offset = Vector3(0,5,0)
	set_translation(offset)
