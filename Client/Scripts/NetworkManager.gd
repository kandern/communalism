extends Node

signal player_joined
signal player_left
signal connection_successful
signal ready_up

var SERVER_IP = "127.0.0.1"
var SERVER_PORT = 6969
var userName = "Player"
var id = -1
var players = []
var nextScene = ""

func _ready():
	if OS.has_environment("USERNAME"):
		userName = OS.get_environment("USERNAME")
	#The server has no need for this script
	if OS.has_feature("server"):
		queue_free()
	else:
		name = "NetworkManager"
		get_tree().connect("connected_to_server", self, "connection_Successful")

func on_Connect():
	var peer = NetworkedMultiplayerENet.new()
	peer.create_client(SERVER_IP, SERVER_PORT)
	get_tree().network_peer = peer
	id = get_tree().get_network_unique_id()

func Disconnect():
	get_tree().network_peer = null

func connection_Successful():
	rpc_id(1,"register_player", userName)
	emit_signal("connection_successful")

func on_ready():
	rpc_id(1,"ready_up")

#The following functions are executed by the server remotely
remote func ready_up(id):
	var index = -1
	for player in players:
		if player.id==id:
			index = players.find(player)
	if index>=0:
		players[index].ready = true
	emit_signal("ready_up")
	
remote func player_left(id):
	var index = -1
	for player in players:
		if player.id==id:
			index = players.find(player)
	if index>=0:
		players.remove(index)
	emit_signal("player_left")
remote func player_joined(PlayerList):
	for player in PlayerList:
		if(!players.has(player)):
			players.append(player)
	emit_signal("player_joined")
remote func change_scene(scene):
	get_tree().change_scene(scene)
