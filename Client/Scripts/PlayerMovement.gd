extends KinematicBody

var velocity = Vector3.ZERO
export var SPEED = 5
var size = 1.0
func _ready():
	spawn()

func onCollision(collision):
	if collision.collider.has_method("hit"):
		collision.collider.hit(velocity)
	
func _physics_process(delta):
	scale.x = lerp(scale.x,size,.01)
	scale.y = lerp(scale.x,size,.01)
	scale.z = lerp(scale.x,size,.01)
	var vy = velocity.y
	if Input.is_action_pressed("forward"):
		velocity = -transform.basis.z * SPEED
	elif Input.is_action_pressed("backward"):
		velocity = transform.basis.z * SPEED
	else:
		velocity = lerp(velocity,Vector3.ZERO,.1)
	if Input.is_action_pressed("right"):
		rotate_y(-SPEED/2 * delta)
	if Input.is_action_pressed("left"):
		rotate_y(SPEED/2 * delta)
	velocity.y = vy
	velocity.y = max(velocity.y-.2,-10)
	var collisions = move_and_slide(velocity*SPEED,Vector3.UP,false,4,1,false)

	for i in get_slide_count():
		if get_slide_collision(i).collider is StaticBody:
			pass
		else:
			onCollision(get_slide_collision(i))

	if Input.is_action_pressed("jump") and is_on_floor():
		velocity.y = 7
	if(translation.y<-10):
		spawn()
	get_tree().get_root().get_node("Lobby").sendTransform(transform)

func getSize():
	return size

func spawn():
	var offset = Vector3(0,5,0)
	set_translation(offset)
