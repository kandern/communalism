extends Camera

export(NodePath) var poi
var destination
export(NodePath) var playerPath
var player
var playerSize = 1
var offset = Vector3(0,5,10)
var position = Vector3(0,0,0)
# Called when the node enters the scene tree for the first time.
func _ready():
	#Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	#Input.setmousemode(Input.MOUSEMODEVISIBLE)
	destination = get_node(poi)
	player = get_node(playerPath)
	position = destination.get_global_transform().origin
	position += offset

func _input(event):         
	if event is InputEventMouseMotion:
		offset.x += event.relative.x*.05
		offset.x = min(max(-5,offset.x),5)

func _physics_process(delta):
	if Input.is_action_pressed("ui_cancel"):
		get_tree().quit()
	var newPos: Vector3 = destination.get_global_transform().origin

	if(player.has_method("getSize")):
		playerSize = player.getSize()
		newPos.x = newPos.x + playerSize
		newPos.y = newPos.y + playerSize
		newPos.z = newPos.z + playerSize
	#newPos += offset
	#newPos.y = newPos.y-fmod(newPos.y,4)
	position.x = lerp(position.x,newPos.x,.1)
	position.y = lerp(position.y,newPos.y,.1)
	position.z = lerp(position.z,newPos.z,.1)
	look_at(player.get_global_transform().origin,Vector3.UP)
	set_global_transform(Transform(get_global_transform().basis,position))
