extends HBoxContainer

func _ready():
	pass 

func populate(name,ready):
	if ready:
		$lblReady.set_text("Ready")
	else:
		$lblReady.set_text("Not Ready")
	$lblName.set_text(name)

func ready():
	$lblReady.set_text("Ready")
